import java.util.Scanner;

public class lab03 {
    public static int searchIndex(int[] arr,int target){
        int left = 0;
        int right = arr.length-1;

        while (left <= right) {
            int mid = left + (right - left) / 2;

            if (arr[mid] == target) {
                return mid; // Return the index of the target element
            } else if (arr[mid] < target) {
                left = mid + 1; // Discard the left half of the array
            } else {
                right = mid - 1; // Discard the right half of the array
            }
        }

        return -1; // Return -1 if the target element is not found
    }

    public static void orderArray(int[] arr){
        int swpNum;
        for(int i = 0;i<arr.length;i++){          
            for(int j = i+1;j<arr.length;j++){
                if(arr[i]>arr[j]){
                    swpNum = arr[i];
                    arr[i] = arr[j];
                    arr[j] = swpNum;
                }
            }
        }
    }

    //private static void printArray(int[] arr){
    //    for(int i=0;i<arr.length;i++){
    //        System.out.print(arr[i]+" ");
    //    }
    //    System.out.println();
    //}
    
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int[] nums = {4,5,6,7,0,1,2};
        int target;

        orderArray(nums);
        target = sc.nextInt();   

        int index = searchIndex(nums, target);

        if (index != -1) {
            System.out.println("Element " + target + " found at index " + index);
        } else {
            System.out.println("Element " + target + " not found in the array");
        }
    }
}
